To use the app you will need to have an OpenWeatherMap API key.

https://home.openweathermap.org/users/sign_up

Once you have the key you can paste it into 

    doc/open-weather-map-api-key.ts.example

And copy that file to 

    cp doc/open-weather-map-api-key.ts.example src/service/open-weather-map-api-key.ts
    
Alternatively just use this command to directly create the file.

    echo "export const APPID = \"YOUR_API_KEY_GOES_HERE\";" > src/service/open-weather-map-api-key.ts
