import React, {Component} from 'react';
import LocationInput from "./components/LocationInput";
import WeatherReportView from './components/WeatherReport';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import {WeatherService, OpenWeatherResponse} from './service/weather-service';
import {City, DailyForecast} from './service/weather-service-types';
import './App.css';

const weatherService = new WeatherService();

interface AppState {
    dailyForecasts: DailyForecast[];
    isLoadingData: boolean;
    city?: City;
    hasError?: boolean;
    errorMessage?: string;
}

class App extends Component<any, AppState> {
    
    constructor(props: any) {
        super(props);
        this.state = {
            dailyForecasts: [],
            isLoadingData: false,
            city: {
                id: "2964574",
                name: "Dublin",
                country: "IE",
                coord: {
                    lon: -6.2603,
                    lat: 53.3498
                }
            },
        };
    }

    getWeather(value: any) {
        this.setState({
            dailyForecasts: [],
            isLoadingData: true,
            hasError: false,
        });

        weatherService.getWeather(value)
            .then(result => {
                if (result.hasError()) this.handleError(result.getErrorMessage());
                else this.handleResult(result);
            })
            .catch(e => {
                console.log(e);
                this.handleError(e);
            });
    }

    handleError(errorMessage: any) {
        this.setState({
            hasError: true,
            errorMessage: errorMessage,
            isLoadingData: false
        });
    }

    handleResult(result: OpenWeatherResponse) {
        this.setState({
            dailyForecasts: result.getDailyForecastsGrouped(),
            city: result.getCity(),
            isLoadingData: false
        });
    }

    onLocationContextChange(locationContext: string) {
        if (locationContext == "cc") {
            this.setState({dailyForecasts: []});
        }
    }

    render() {
        const mainContent = this.renderMainContent();
        return (
            <div className="App">
                <Container className="App-container">
                    <Row className="App-input">
                        <Col sm={12}>
                            <LocationInput
                                onSubmit={(value) => this.getWeather(value)}
                                onLocationContextChange={(value) => this.onLocationContextChange(value)}
                                city={this.state.city}
                            />
                        </Col>
                    </Row>
                    <Row className="App-main">
                        <Col sm={12}>
                            {mainContent}
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }

    renderMainContent() {
        if (this.state.isLoadingData)
            return (
                <div className="App-loading">
                    Loading...
                </div>
            );

        if (this.state.hasError)
            return <div className="App-error-message">{this.state.errorMessage}</div>

        if (this.state.dailyForecasts.length > 0)
            return <WeatherReportView dailyForecasts={this.state.dailyForecasts} city={this.state.city}/>
    }
}

export default App;
