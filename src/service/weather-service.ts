import {DailyForecast, Forecast, ForecastGroup, WeatherType, City} from './weather-service-types';
import {APPID} from './open-weather-map-api-key';

const base_url = "http://api.openweathermap.org/data/2.5/forecast";

export interface WeatherByCityAndCountryCodeParams {
    city: string;
    countryCode: string;
}

export interface WeatherByCityParams {
    q: string;
}

export interface WeatherByCityIdParams {
    id: string;
}

export interface WeatherByCoordsParams {
    lat: string;
    lon: string;
}

export interface LocationContextParams {
    locationContext:string;
    [key:string]:string;
}

export class WeatherService {

    getWeather(params:LocationContextParams) {
        switch (params.locationContext) {
            case "cc":
                return this.getWeatherByCity({q: `${params.city},${params.countryCode}`});
            case "ll":
                return this.getWeatherByCoords({lat: params.lat, lon: params.lon});
            default:
                return Promise.reject("unsupported `locationContext`");
        }
    }

    getWeatherByCityAndCountryCode(params: WeatherByCityAndCountryCodeParams): Promise<OpenWeatherResponse> {
        const url = this.constructUrl({q: `${params.city},${params.countryCode}`});
        return this.doFetch(url);
    }

    getWeatherByCity(params: WeatherByCityParams): Promise<OpenWeatherResponse> {
        const url = this.constructUrl(params);
        return this.doFetch(url);
    }

    getWeatherByCityId(params: WeatherByCityIdParams): Promise<OpenWeatherResponse> {
        const url = this.constructUrl(params);
        return this.doFetch(url);
    }

    getWeatherByCoords(params: WeatherByCoordsParams): Promise<OpenWeatherResponse> {
        const url = this.constructUrl(params);
        return this.doFetch(url);
    }

    private constructUrl(params: any) {
        params.APPID = getApiKey();
        let keys = Object.keys(params);
        // OpenWeatherMap API doesn't appreciate encoded uri components.
        //let keysAndValues = keys.map(k=>`${k}=${encodeURIComponent(params[k])}`);
        let paramList = keys.map(k => `${k}=${params[k]}`);
        return `${base_url}?${paramList.join("&")}`;
    }

    private doFetch(url: string) {
        return fetch(url)
            .then((response) => response.json())
            .then((json) => new OpenWeatherResponse(json))
            .catch(err => {
                console.error("OpenWeatherMap returned an Error:");
                console.error(err);
                throw new Error(err);
            });
    }
}



export class OpenWeatherResponse {
    private readonly response: any;

    constructor(response: any) {
        this.response = response;
    }

    getErrorMessage():string {
        return this.response.message;
    }

    hasError():boolean {
        return !this.response || this.response.cod != "200";
    }

    getCity():City {
        return <City>this.response.city;
    }

    getForecasts(): Forecast[] {
        if (!this.response || this.response.cod != "200") return [];
        return this.response.list.map((v: any) => {
            return {
                date: new Date(v.dt * 1000),
                dt: v.dt,
                temp: v.main.temp,
                low: v.main.temp_min,
                high: v.main.temp_max,
                weatherTypes: v.weather.map((item: any, i: number) => {
                    return {
                        id: v.dt + '.' + i,
                        icon: item.icon,
                        description: item.description,
                        status: item.main
                    }
                })
            }
        });
    }

    getDailyForecasts(): DailyForecast[] {
        const reports = this.getForecasts();
        const datesMap = new Map<string, Forecast[]>();
        let report, date;
        for (report of reports) {
            date = getDateKey(report.date);
            if (!datesMap.has(date)) datesMap.set(date, []);
            // @ts-ignore
            datesMap.get(date).push(report);
        }

        let dailyForecastList: DailyForecast[] = [],
            dailyForecast: DailyForecast;

        datesMap.forEach((v, k) => {
            dailyForecast = <DailyForecast>{};
            dailyForecast.forecasts = this.padForecasts(v);
            dailyForecast.date = k;
            dailyForecastList.push(dailyForecast);
        });
        return dailyForecastList;
    }

    private padForecasts(forecasts:Forecast[]) {

        let first:Forecast = forecasts[0];
        const padDirection = first.date.getHours() > 0 ? 1:-1;

        // only need to left pad.
        if(padDirection > 0) {
            while(forecasts.length < 8) {
                let forecast = forecasts[0];
                let previousDt = forecast.dt - 60 * 60 * 3;
                forecasts.unshift({
                    dt:previousDt,
                    date: new Date(previousDt*1000),
                    weatherTypes:[]
                });
            }
        }

        return forecasts;
    }

    /**
     * Get the daily forecasts grouped into [morning, afternoon, evening, night]
     */
    getDailyForecastsGrouped(): DailyForecast[] {
        const dailyForecasts = this.getDailyForecasts();
        let groupedDailyForecasts = dailyForecasts.map(v=> {
            let groupedForecasts = [];
            let forecasts = v.forecasts.slice();
            while(forecasts.length > 1) {
                groupedForecasts.push(
                    this.createForecastGroup(<Forecast>forecasts.shift(), <Forecast>forecasts.shift())
                );
            }
            v.grouped = groupedForecasts;
            return v;
        });

        let i,
            day0,
            day1;

        for(i = groupedDailyForecasts.length -1; i >=  1; i--) {
            day1 = groupedDailyForecasts[i];
            day0 = groupedDailyForecasts[i-1];

            if(day0) {
                let night = <ForecastGroup>day1.grouped.shift();
                day0.grouped.push(night);
            }
        }
        // remove leading night period;
        if(day0) day0.grouped.shift();


        // aggregate daily temperature highs and lows
        groupedDailyForecasts.forEach((day:DailyForecast) => {
            let high:number = Number.MIN_VALUE,
                low:number = Number.MAX_VALUE;

            day.grouped.forEach((group:ForecastGroup)=>{
                group.forecasts.forEach((forecast:Forecast) => {
                    if(forecast.high && forecast.high > high) high = forecast.high;
                    if(forecast.low && forecast.low < low) low = forecast.low;
                })
            });
            day.high = high;
            day.low = low;
        });

        return groupedDailyForecasts.slice(0,5);
    }



    private createForecastGroup(forecast0:Forecast, forecast1:Forecast):ForecastGroup {

        let weatherTypes:WeatherType[] = [];
        weatherTypes = weatherTypes.concat(forecast0.weatherTypes,forecast1.weatherTypes);

        return {
            forecasts:[forecast0, forecast1],
            weatherTypes:weatherTypes,
            date:forecast0.date,
            dt:forecast0.dt
        };
    }
}

function getDateKey(date: Date): string {
    return `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`
}

function getApiKey() {
    if(APPID) return APPID;

    const params = new URLSearchParams(window.location.search);
    if(!params.has('appid')) throw new Error("please specify an `appid=XXXX` url parameter");
    console.log("appid", params.get('appid'));
    return params.get('appid');
}
