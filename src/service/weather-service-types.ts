export interface DailyForecastList {
    dailyForecasts:DailyForecast[];
    city?:City;
}

export interface City {
    id:string;
    name:string;
    coord:{
        lat:number;
        lon:number;
    },
    country:string;
}

export interface DailyForecast {
    date: string;
    high:number;
    low:number;
    forecasts:Forecast[];
    grouped:ForecastGroup[];
}

export interface Forecast {
    date: Date;
    dt:number;
    high?:number;
    low?:number;
    weatherTypes: WeatherType[];
}

export interface  ForecastGroup {
    date:Date;
    dt:number;
    forecasts:Forecast[];
    weatherTypes: WeatherType[];
}

export interface WeatherType {
    id:string;
    icon: string;
    description: string;
    status: string;
}