import {OpenWeatherResponse} from './weather-service';
import result from '../../docs/open-weather-map-api-response.json';

let response:OpenWeatherResponse;

describe("OpenWeatherResponse", ()=>{
    it("should not throw error", ()=>{
        response = new OpenWeatherResponse(result);
        expect(response).toBeInstanceOf(OpenWeatherResponse);
    });

    it("should return 5 * 8 (40) Forecasts", ()=>{
        let forecasts = response.getForecasts();
        expect(forecasts).toHaveLength(40);
    });

    it("should return 5 DailyForecastGroups", ()=>{
        let forecastGroups = response.getDailyForecastsGrouped();
        expect(forecastGroups).toHaveLength(5);
    });
});


