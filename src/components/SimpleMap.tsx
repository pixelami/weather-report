import React, { Component } from 'react';
import GoogleMapReact from 'google-map-react';
import './SimpleMap.css';

const MAP_KEY = "AIzaSyDo8QiCLX26rCd7ViVJQPkyuOlLSireLqo";

interface SimpleMapProps {
    center:{lat:number, lng:number};
    zoom:number;
    onChange:(evt:any)=>void;
    height:string
}

class SimpleMap extends Component<SimpleMapProps> {
    static defaultProps = {
        center: {
            lat: 53.3498,
            lng: -6.2603
        },
        zoom: 6
    };

    render() {
        return (
            <div className="SimpleMap" style={{ height: this.props.height}}>
                <GoogleMapReact
                    bootstrapURLKeys={{ key: MAP_KEY }}
                    defaultCenter={this.props.center}
                    defaultZoom={this.props.zoom}
                    onChange={(evt:any)=>this.props.onChange(evt)}
                >
                </GoogleMapReact>
            </div>
        );
    }
}

export default SimpleMap;