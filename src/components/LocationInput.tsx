import React, {Component, FormEvent} from 'react';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import InputGroup from 'react-bootstrap/InputGroup';
import DropdownButton from 'react-bootstrap/DropdownButton';
import Dropdown from 'react-bootstrap/Dropdown';
import SimpleMap from "./SimpleMap";
import {City} from "../service/weather-service-types";

interface LocationInputState {
    locationContext: string;
    city?: string;
    countryCode?: string;
    lon?: string;
    lat?: string;
}

interface LocationInputProps {
    onSubmit: (value: any) => void;
    onLocationContextChange: (value: any) => void;
    cityName?: string;
    countryCode?: string;
    city?:City;
}

class LocationInput extends Component<LocationInputProps, LocationInputState> {

    static defaultProps = {
        cityName:"Dublin",
        countryCode:"ie"
    };

    constructor(props: LocationInputProps) {
        super(props);
        this.state = this.getDefaultState();
    }

    getDefaultState() {
        const state:LocationInputState = {
            locationContext: "cc",
            city: this.props.cityName,
            countryCode: this.props.countryCode
        };
        if(this.props.city) {
            state.lat = this.props.city.coord.lat.toString();
            state.lon = this.props.city.coord.lon.toString();
        }
        return state;
    }

    handleSubmit(event: FormEvent) {
        event.preventDefault();

        let payload: any = {locationContext: this.state.locationContext};
        if (this.state.locationContext == 'cc') {
            payload.city = this.state.city;
            payload.countryCode = this.state.countryCode;
        } else {
            payload.lat = this.state.lat;
            payload.lon = this.state.lon;
        }
        this.props.onSubmit(payload);
    }

    handleChange(event: FormEvent<HTMLInputElement>) {
        const target:any = event.target;
        const newState: any = {[target.name]: (target as HTMLInputElement).value};
        this.setState(Object.assign({}, this.state, newState));
    }

    handleLocationContextSelect(event: string) {
        if(event == "ll") {
            console.log('props.city', this.props.city);
        }
        this.setState({locationContext:event});
        this.props.onLocationContextChange(event);
    }

    handleMapChange(event: any) {
        const centerCoords = event.center;
        console.log(centerCoords);
        this.setState({
            lat: centerCoords.lat,
            lon: centerCoords.lng
        });
    }

    render() {
        const inputs = this.renderInputs();
        const mapContent = this.renderMap();
        return (
            <div>
                <Form onSubmit={(ev: FormEvent) => this.handleSubmit(ev)}>
                    <InputGroup>
                        <DropdownButton
                            as={InputGroup.Prepend}
                            variant="outline-secondary"
                            title="Location"
                            id="input-group-dropdown-1"
                            onSelect={(ev: string) => this.handleLocationContextSelect(ev)}
                        >
                            <Dropdown.Item
                                href="#"
                                eventKey="cc"
                                active={this.state.locationContext == "cc"}>
                                City / Country Code
                            </Dropdown.Item>
                            <Dropdown.Item
                                href="#"
                                eventKey="ll"
                                active={this.state.locationContext == "ll"}>
                                Latitude / Longitude
                            </Dropdown.Item>
                        </DropdownButton>
                        {inputs}
                        <InputGroup.Append>
                            <Button variant="primary" type="submit" block>
                                Submit
                            </Button>
                        </InputGroup.Append>
                    </InputGroup>
                </Form>
                {mapContent}
            </div>

        )
    }

    renderInputs() {
        if (this.state.locationContext == "cc") {
            return (
                <>
                    <Form.Control
                        placeholder="City"
                        name="city"
                        value={this.state.city}
                        type="text"
                        onChange={(ev: any) => this.handleChange(ev)}
                    />
                    <Form.Control
                        placeholder="Country Code"
                        name="countryCode"
                        value={this.state.countryCode}
                        type="text"
                        onChange={(ev: any) => this.handleChange(ev)}
                    />
                </>
            );
        }
        return (
            <>
                <Form.Control
                    placeholder="Latitude"
                    name="lat"
                    value={this.state.lat}
                    type="text"
                    onChange={(ev: FormEvent<any>) => this.handleChange(ev)}
                />
                <Form.Control
                    placeholder="Longitude"
                    name="lon"
                    value={this.state.lon}
                    type="text"
                    onChange={(ev: any) => this.handleChange(ev)}
                />
            </>
        );


    }

    renderMap() {
        if (this.state.locationContext == "ll") {
            let center:any;
            if(this.props.city) {
                center = {};
                center.lat = this.props.city.coord.lat;
                center.lng = this.props.city.coord.lon;
            }
            return (
                <SimpleMap
                    height="400px"
                    onChange={(evt: any) => this.handleMapChange(evt)}
                    zoom={8}
                   center={center}
                />

            )
        }
    }
}

export default LocationInput;