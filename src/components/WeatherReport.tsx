import React, {Component} from 'react';
import {format} from 'date-fns';
import {
    Forecast,
    WeatherType,
    DailyForecast,
    DailyForecastList,
    ForecastGroup,
} from '../service/weather-service-types';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import './WeatherReport.css';

interface WeatherReportViewState {
    hasError: boolean;
}

class WeatherReportView extends Component<DailyForecastList, WeatherReportViewState> {

    static getDerivedStateFromError(error: Error) {
        return {hasError: true};
    }

    constructor(props: DailyForecastList) {
        super(props);
        this.state = {hasError: false};
    }

    componentDidCatch(error: Error, info: any) {
        console.error(error, info);
    }

    render() {

        if (this.state.hasError) {
            return <div>Unable to show results</div>
        }

        const title = this.renderTitle();
        const dailyForecasts = this.renderDailyForecasts(this.props.dailyForecasts);

        return <div className="WeatherReport">
            {title}
            {dailyForecasts}
        </div>;
    }

    renderDailyForecasts(days: DailyForecast[]) {
        return days.map(v => <DailyForecastView key={v.date} {...v}/>)
    }

    renderTitle() {
        if (this.props.city && this.props.city.id) {
            const {name, country} = this.props.city;
            return (
                <div className="WeatherReport-title">
                    <h4>{name}
                        <small> ({country})</small>
                    </h4>
                    5 day forecast
                </div>
            );
        }
    }
}

class DailyForecastView extends Component<DailyForecast> {
    render() {
        //const forecasts = this.renderForecasts(this.props.forecasts);
        const forecasts = this.renderForecastGroups(this.props.grouped);

        return (
            <Row noGutters={true} className="DailyForecast">
                <Col className="DailyForecast-date text-nowrap" xs={2}>
                    <span>{formatDay(this.props.date)} </span>
                    <span><strong>{formatDate(this.props.date)}</strong></span>
                </Col>
                <Col className="DailyForecast-temp" xs={2}>
                    <div className="DailyForecast-high text-nowrap">{formatTemp(this.props.high)}</div>
                    <div className="DailyForecast-low text-nowrap">{formatTemp(this.props.low)}</div>
                </Col>
                {forecasts}
            </Row>
        );
    }

    renderForecasts(forecasts: Forecast[]) {
        return forecasts.map(v => <ForecastView key={v.dt} {...v}/>);
    }

    renderForecastGroups(forecastGroups: ForecastGroup[]) {
        return forecastGroups.map(v => <ForecastGroupView key={v.dt} {...v}/>);
    }
}

class ForecastGroupView extends Component<ForecastGroup> {
    render() {
        if (this.props.weatherTypes.length < 1) return <Col className="Forecast" xs={2}/>;

        const weatherTypes = this.renderWeatherTypes(this.props.weatherTypes);

        return (
            <Col className="Forecast" xs={2}>
                <div className="Forecast-time">
                    <small>{formatTime(this.props.date)}</small>
                </div>
                <div className="Forecast-weather">
                    {weatherTypes}
                </div>
            </Col>
        );
    }

    renderWeatherTypes(weatherTypes: WeatherType[]) {
        // since the weather types are being grouped we will show the user
        // the worst case scenario for this part of the day.
        const worstWeatherType = getWorstWeatherType(weatherTypes);
        return <WeatherTypeView {...worstWeatherType}/>;
    }
}

class ForecastView extends Component<Forecast> {
    render() {
        if (this.props.weatherTypes.length < 1) return <div className="Forecast"/>;

        const weatherTypes = this.renderWeatherTypes(this.props.weatherTypes);

        return (
            <div className="Forecast">
                <div>{formatTime(this.props.date)}</div>
                <div>{weatherTypes}</div>
            </div>
        );
    }

    renderWeatherTypes(weatherTypes: WeatherType[]) {
        return weatherTypes.map(v => <WeatherTypeView key={v.id} {...v}/>);
    }
}

class WeatherTypeView extends Component<WeatherType> {
    render() {
        return <div className="WeatherType">
            <img
                src={"http://openweathermap.org/img/w/" + this.props.icon + ".png"}
                alt={this.props.description}
            />
        </div>;
    }
}


function formatTime(date: Date) {
    return format(date, "HH:mm");
}

function formatDay(date: string) {
    return format(date, "ddd");
}

function formatDate(date: string) {
    return format(date, "D");
}

function getWorstWeatherType(weatherTypes: WeatherType[]) {

    let idx = 0;

    if (weatherTypes.length > 1) {
        for (let i = 1; i < weatherTypes.length; i++) {
            let a = weatherTypes[idx].icon.slice(0, 2);
            let b = weatherTypes[i].icon.slice(0, 2);
            if (b > a) {
                idx = i;
            }
        }
    }

    return weatherTypes[idx];
}

const absoluteZero = 273.15;

function formatTemp(temp: number) {
    return Math.round(temp - absoluteZero) + " C";
}

export default WeatherReportView;