- Define layout for very small screens
- Add toggleable user help / example usage for each location context.
- Add tests (especially for weather-service).
- Add more rigorous testing of the grouping of the 8 * 3 hour forecast intervals.
- Implement correct selection of the worst 3 hour weather forecast when grouping forecasts.
- Add google map for latitude/longitude selection.
- Add loading spinner animation.
- Add input validation (e.g. missing country code).
- Add auto-complete for city names/ids via 'city.list.json.gz'
- Refactor form submission handling.
- Possibly switch to redux store.
- Change icons.
- Embed icons in build.
- Add locations history.